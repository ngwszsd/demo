// 获取可用菜单
export interface GetMenus {
  title?: string,
  key?: string,
  url?: string,
  icon?: string,
  imagePath?: string,
  order?: string,
  Parameters?: string,
  OpNodes?: string,
  children?: Array
  MenuID?: number,
  MenuLevel?: number,
  ParentID?: number,
}
// 点击一个菜单调用runAPPFunction
export interface ClickMenu{
}