// 获取角色清单
export interface RolesData {
    Leaf?: number
    Code?: string
    Name?: string
}
// 选择角色时调用本接口
export interface SelectRole {
}
// 获取可用系统
export interface GetSystems{
    BackgroundPath?: string
    CommunityID?: number
    IconName?: string
    ImagePath?: string
    MenuStyle?: number
    Ordinal?: number
    PortalUrl?: string
    ShortName?: string
    SystemID?: number
    SystemName?: string
    Terminal?: string
    TopBannerPath?: string
    Valid?: boolean
    VersionNo?: string
}
// 退出系统
export interface logout {
}