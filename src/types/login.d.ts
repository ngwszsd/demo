// 获取社区信息
export interface CommunityData {
    CommunityID?: number
    CommunityName?: string
    DunsCode?: string
    RegistYear?: number
    T1NodeID?: number
    T1002LeafID?: number
    LoginDivTop?: number
    LoginDivLeft?: number
}
// 获取用户机构清单
export interface GetAgencies{
    Leaf?: number
    Code?: string
    Name?: string
}
// 验证用户密码
export interface UserPWD{
}
// 用户登录
export interface Login{
    ErrCode?: number,
    ErrText?: string,
    UserName?: string,
    NickName?: string,
    SysLogID?: number,
    access_token?: string,
    LanguageID?: number,
    OPhoneNo?: string,
    HPhoneNo?: string,
    MPhoneNo?: string,
    AgencyName?: string,
    HostName?: string,
}