import axios from '../utils/http'
import type { ChangePWD } from '~/types/pwd'

// 修改用户密码
export const fetchChangePWD = (params: object) => {
  return axios.post<ChangePWD>('/api/Admin/ChangePWD', params)
}
