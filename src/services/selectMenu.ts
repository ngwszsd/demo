import axios from '../utils/http'
import type { ClickMenu, GetMenus } from '~/types/selectMenu'

// 获取可用菜单
export const fetchGetMenus = (params: object) => {
  return axios.post<GetMenus>('/api/Admin/GetMenus', params)
}

// 点击菜单调用runAFunction
export const fetchClickMenu = (params: object) => {
  return axios.post<ClickMenu>('/api/Admin/ClickMenu', params)
}

