import axios from '../utils/http'
import type { GetSystems, RolesData, SelectRole, logout } from '~/types/selectSystem'

// 获取角色清单
export const fetchGetRolesData = (communityID: number, userCode: string) => {
  return axios.get<RolesData>(`/api/Admin/GetRoles?communityID=${communityID}&userCode=${userCode}`)
}

// 选择角色时调用本接口
export const fetchSelectRole = (params: object) => {
  return axios.post<SelectRole>('/api/Admin/SelectRole', params)
}

// 获取可用系统
export const fetchGetSystems = () => {
  return axios.get<GetSystems>('/api/Admin/GetSystems')
}

// 退出系统
export const fetchLogout = () => {
  return axios.post<logout>('/api/Admin/Logout', { remark: 'out' })
}

