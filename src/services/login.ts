import axios from '../utils/http'
import type { CommunityData, GetAgencies, Login, UserPWD } from '~/types/login'

// 获取用户社区号
export const fetchGetCommunity = (communityID: number) => {
  // 返回的数据格式可以和服务端约定
  return axios.get<CommunityData>(`/api/Admin/GetCommunityInfo?communityID=${communityID}`)
}

// 验证用户密码
export const fetchUserPWD = (params: object) => {
  return axios.post<UserPWD>('/api/Admin/UserPWDVerify', params)
}
// 获取用户机构清单
export const fetchGetAgencies = (communityID: number, userCode: string) => {
  return axios.get<GetAgencies>(`/api/Admin/GetAgencies?communityID=${communityID}&userCode=${userCode}`)
}

// 用户登录
export const fetchLogin = (params: object) => {
  return axios.post<Login>('/api/Admin/Login', params)
}
