import { Breadcrumb, BreadcrumbItem, Button } from 'ant-design-vue'
import { defineComponent, withModifiers } from 'vue'
import { fetchGetCommunity } from '~/services/login'

const Text = defineComponent({
  setup() {
    const count = ref(0)

    const inc = () => {
      count.value++
    }

    /**
     *
     */
    const getCommunity = async () => {
      const res = await fetchGetCommunity(60066)
    }

    return () => (
      <Breadcrumb>
        <BreadcrumbItem>Home</BreadcrumbItem>
        <BreadcrumbItem><a onClick={getCommunity}>Application Center</a></BreadcrumbItem>
        <BreadcrumbItem><a onClick={inc}>111{{ count }}</a></BreadcrumbItem>
        <BreadcrumbItem>An Application</BreadcrumbItem>家中有事，请假三天
      </Breadcrumb>
    )
  },
})

export default Text
